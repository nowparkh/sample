#pragma once

#include "../Common/ViewBase.h"

/////////////////////////////////////////////////////////////////////////////
// CThumbnailView window

class CThumbnailItem;
class CThumbnailView : public CViewBase
{
    DECLARE_DYNAMIC(CThumbnailView)
private:
    std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> click;
    std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> over;

    int m_seperatorPixel = { 10 };

    std::unordered_map<UINT, std::shared_ptr< CThumbnailItem>> m_mapThunailImages;

    std::weak_ptr< CThumbnailItem> m_preSelectedItem;
    std::weak_ptr< CThumbnailItem> m_selectedItem;
    std::weak_ptr< CThumbnailItem> m_overedItem;
    std::weak_ptr< CThumbnailItem> m_preOveredItem;

    long              m_startX{ 0 };
    long              m_startY{ 0 };

    long              m_col{ 0 };
    long              m_row{ 0 };

    long              m_thumbWidth{ 120 };
    long              m_thumbHeight{ 120 };


public:
    CThumbnailView();
    virtual ~CThumbnailView();

    virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);

    void Clear();
    void Add(std::shared_ptr< CThumbnailItem> btn);

    void ItemClick(int id);
    void ItemOver(int id);
    void ItemLeave();

    void SetClickTrigger(std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> fClick);
    void SetOverTrigger(std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> fHover);
private:
    void RecalScrollBars( void );
    void RecalButtonPos( void );

protected:
    int GetScrollPos32(int nBar, BOOL bGetTrackPos = FALSE );
    BOOL SetScrollPos32( int nBar, int nPos, BOOL bRedraw = TRUE );
    virtual void OnCustomDraw(Gdiplus::Graphics& gr);
    virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(226, 226, 226); }

public:
    
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CThumbnailView)
    //}}AFX_VIRTUAL

	//*************************************
	//*  Generated message map functions	*
	//*************************************
  protected:
    //{{AFX_MSG(CThumbnailView)
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };


