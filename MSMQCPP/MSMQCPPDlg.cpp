
// MSMQCPPDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "MSMQCPP.h"
#include "MSMQCPPDlg.h"
#include "afxdialogex.h"
#include <Mq.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMSMQCPPDlg dialog



CMSMQCPPDlg::CMSMQCPPDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MSMQCPP_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMSMQCPPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMSMQCPPDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CMSMQCPPDlg message handlers

#define MAX_FORMAT_NAME_LEN 255
HRESULT CMSMQCPPDlg::ReadMSMQQueue()
{
	
	HRESULT hr;
	WCHAR wcsFormatName[MAX_FORMAT_NAME_LEN + 1];
	if (!LocateQueue(m_strLogin, wcsFormatName, sizeof(wcsFormatName) / sizeof(wcsFormatName[0])))
	{
		//
		// Form the pathname of the receiving queue.
		//
		char mbsPathName[MQ_MAX_Q_NAME_LEN + 1];
		DWORD dwNumChars = sizeof(mbsPathName);
		GetComputerName(mbsPathName, &dwNumChars);
		strcat_s(mbsPathName, sizeof(mbsPathName), "\\");
		strcat_s(mbsPathName, sizeof(mbsPathName), m_strLogin);


		//
		// Prepare the receiving queue properties.
		//
		DWORD cProps = 0;
		QUEUEPROPID  aPropId[3];
		MQPROPVARIANT aPropVar[3];
		MQQUEUEPROPS propsQueue;

		WCHAR wcsPathName[MQ_MAX_Q_NAME_LEN + 1];
		size_t nResultedStringLength = 0;
		mbstowcs_s(
			&nResultedStringLength,
			wcsPathName,
			sizeof(wcsPathName) / sizeof(wcsPathName[0]),
			mbsPathName,
			sizeof(wcsPathName) / sizeof(wcsPathName[0]) - 1
		);
		aPropId[cProps] = PROPID_Q_PATHNAME;
		aPropVar[cProps].vt = VT_LPWSTR;
		aPropVar[cProps].pwszVal = wcsPathName;
		cProps++;

		aPropId[cProps] = PROPID_Q_TYPE;
		aPropVar[cProps].vt = VT_CLSID;
		aPropVar[cProps].puuid = &guidDrawType;
		cProps++;

		WCHAR wcsLabel[MQ_MAX_Q_LABEL_LEN + 1];
		nResultedStringLength = 0;
		mbstowcs_s(
			&nResultedStringLength,
			wcsLabel,
			sizeof(wcsLabel) / sizeof(wcsLabel[0]),
			m_strLogin,
			sizeof(wcsLabel) / sizeof(wcsLabel[0]) - 1
		);
		aPropId[cProps] = PROPID_Q_LABEL;
		aPropVar[cProps].vt = VT_LPWSTR;
		aPropVar[cProps].pwszVal = wcsLabel;
		cProps++;

		propsQueue.cProp = cProps;
		propsQueue.aPropID = aPropId;
		propsQueue.aPropVar = aPropVar;
		propsQueue.aStatus = NULL;


		//
		// Create the receiving queue.
		//
		dwNumChars = sizeof(wcsFormatName) / sizeof(wcsFormatName[0]);
		hr = MQCreateQueue(NULL, &propsQueue, wcsFormatName, &dwNumChars);


		//
		// If the receiving queue already exists, obtain its format name.
		//
		if (hr == MQ_ERROR_QUEUE_EXISTS)
		{
			dwNumChars = sizeof(wcsFormatName) / sizeof(wcsFormatName[0]);
			hr = MQPathNameToFormatName(wcsPathName, wcsFormatName, &dwNumChars);
		}

		if (FAILED(hr))
			return FALSE;
	}

	//
	// Open the receiving queue (it may be necessary to retry due to replication latency).
	//
	int iCount = 0;
	while ((hr = MQOpenQueue(wcsFormatName,
		MQ_RECEIVE_ACCESS,
		MQ_DENY_NONE,
		&m_hqIncoming)) == MQ_ERROR_QUEUE_NOT_FOUND)
	{

		Sleep(500);
		iCount++;
		if (iCount >= 30)
		{
			//
			// A period of 15 seconds past without locating the queue.
			// We allow the user to stop attempts to open the queue.
			//
			int iRes = MessageBox("The receiving queue was not located.\nDo you want to continue searching?",
				NULL,
				MB_YESNO);
			if (iRes == IDYES)
			{
				//
				// Continue searching.
				//
				iCount = 0;
			}
			else
			{
				//
				// Stop searching.
				//
				break;
			}
		}
	}

	if (FAILED(hr))
		return FALSE;


	return TRUE;
}

HRESULT CMSMQCPPDlg::CreateMSMQQueue(
	LPWSTR wszPathName,
	PSECURITY_DESCRIPTOR pSecurityDescriptor,
	LPWSTR wszOutFormatName,
	DWORD* pdwOutFormatNameLength
)
{

	// Define the maximum number of queue properties.
	const int NUMBEROFPROPERTIES = 2;


	// Define a queue property structure and the structures needed to initialize it.
	MQQUEUEPROPS   QueueProps;
	MQPROPVARIANT  aQueuePropVar[NUMBEROFPROPERTIES];
	QUEUEPROPID    aQueuePropId[NUMBEROFPROPERTIES];
	HRESULT        aQueueStatus[NUMBEROFPROPERTIES];
	HRESULT        hr = MQ_OK;


	// Validate the input parameters.
	if (wszPathName == NULL || wszOutFormatName == NULL || pdwOutFormatNameLength == NULL)
	{
		return MQ_ERROR_INVALID_PARAMETER;
	}

	DWORD cPropId = 0;
	aQueuePropId[cPropId] = PROPID_Q_PATHNAME;
	aQueuePropVar[cPropId].vt = VT_LPWSTR;
	aQueuePropVar[cPropId].pwszVal = wszPathName;
	cPropId++;

	WCHAR wszLabel[MQ_MAX_Q_LABEL_LEN] = L"Test Queue";
	aQueuePropId[cPropId] = PROPID_Q_LABEL;
	aQueuePropVar[cPropId].vt = VT_LPWSTR;
	aQueuePropVar[cPropId].pwszVal = wszLabel;
	cPropId++;



	QueueProps.cProp = cPropId;               // Number of properties
	QueueProps.aPropID = aQueuePropId;        // IDs of the queue properties
	QueueProps.aPropVar = aQueuePropVar;      // Values of the queue properties
	QueueProps.aStatus = aQueueStatus;        // Pointer to the return status



	WCHAR wszFormatNameBuffer[256];
	DWORD dwFormatNameBufferLength = sizeof(wszFormatNameBuffer) / sizeof(wszFormatNameBuffer[0]);
	hr = MQCreateQueue(pSecurityDescriptor,         // Security descriptor
		&QueueProps,                 // Address of queue property structure
		wszFormatNameBuffer,         // Pointer to format name buffer
		&dwFormatNameBufferLength);  // Pointer to receive the queue's format name length

	if (hr == MQ_OK || hr == MQ_INFORMATION_PROPERTY)
	{
		if (*pdwOutFormatNameLength >= dwFormatNameBufferLength)
		{
			wcsncpy_s(wszOutFormatName, *pdwOutFormatNameLength - 1, wszFormatNameBuffer, _TRUNCATE);

			wszOutFormatName[*pdwOutFormatNameLength - 1] = L'\0';
			*pdwOutFormatNameLength = dwFormatNameBufferLength;
		}
		else
		{
			wprintf(L"The queue was created, but its format name cannot be returned.\n");
		}
	}
	return hr;
}


BOOL CMSMQCPPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	DWORD testLength = 200;
	wchar_t test[200];
	//CreateMSMQQueue(L".\\Private$\\MSMQT", nullptr, test, &testLength);
	ReadMSMQQueue();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMSMQCPPDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMSMQCPPDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMSMQCPPDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

