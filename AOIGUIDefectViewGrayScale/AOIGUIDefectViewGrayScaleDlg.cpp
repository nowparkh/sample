
// AOIGUIDefectViewGrayScaleDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "AOIGUIDefectViewGrayScale.h"
#include "AOIGUIDefectViewGrayScaleDlg.h"
#include "afxdialogex.h"
#include "DefectViewImagePlanes.h"

#include "../SliderRange/GrayScaleSliderControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAOIGUIDefectViewGrayScaleDlg dialog



CAOIGUIDefectViewGrayScaleDlg::CAOIGUIDefectViewGrayScaleDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AOIGUIDEFECTVIEWGRAYSCALE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAOIGUIDefectViewGrayScaleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edtMin);
	DDX_Control(pDX, IDC_SPIN1, m_spinMin);
	DDX_Control(pDX, IDC_EDIT2, m_edtMax);
	DDX_Control(pDX, IDC_SPIN2, m_spinMax);
}

BEGIN_MESSAGE_MAP(CAOIGUIDefectViewGrayScaleDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton6)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, &CAOIGUIDefectViewGrayScaleDlg::OnDeltaposSpin1)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN2, &CAOIGUIDefectViewGrayScaleDlg::OnDeltaposSpin2)
	ON_BN_CLICKED(IDC_BUTTON7, &CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton7)
END_MESSAGE_MAP()


// CAOIGUIDefectViewGrayScaleDlg message handlers

BOOL CAOIGUIDefectViewGrayScaleDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	CRect rect;
	GetClientRect(&rect);
	rect.top = 40;
	rect.bottom = rect.bottom - 40;

	m_imagePlanes = std::make_shared< DefectViewImagePlanes>();
	m_imagePlanes->Create(L"", L"", WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN, rect, this, 1000);
	m_imagePlanes->CreateThumbnailImage(L"C:\\temp\\1_T736\\");

	//CRect rc;
	//rc.left = 50;
	//rc.top = 50;
	//rc.right = rc.left + 768;
	//rc.bottom = rc.top + 160;

	//int w = rc.Width();
	//int h = rc.Height();

	//std::random_device rd;
	//std::mt19937 gen(rd());
	//std::uniform_int_distribution<int> dis(0, 300);

	//std::vector<int> graph;
	//for (int i = 0; i < 255; i++)
	//{
	//	int v = dis(gen);
	//	graph.push_back(v);
	//}

	//m_slider = std::make_shared<GrayScaleSliderControl>();
	//m_slider->SetMoveLeftButtonTrigger(std::bind(&CAOIGUIDefectViewGrayScaleDlg::moveLeftButton, this, std::placeholders::_1));
	//m_slider->SetMoveRightButtonTrigger(std::bind(&CAOIGUIDefectViewGrayScaleDlg::moveRightButton, this, std::placeholders::_1));
	//m_slider->Create(L"", L"", WS_CHILD | WS_VISIBLE, rc, this, IDC_VIEW_GRAYSCALE_SLIDER_CONTROL, nullptr);
	//m_slider->SetGraph(graph);

	//m_spinMin.SetRange(0, 255);
	//m_spinMin.SetPos(0);
	//m_edtMin.SetWindowText(L"0");

	//m_spinMax.SetRange(0, 255);
	//m_spinMax.SetPos(255);
	//m_edtMax.SetWindowText(L"255");

	return TRUE;  // return TRUE  unless you set the focus to a control
}



void CAOIGUIDefectViewGrayScaleDlg::moveLeftButton(int id)
{
	CString str;
	str.Format(L"%d", id);

	m_edtMin.SetWindowText(str);
	m_spinMinValue = id;
	//m_spinMin.SetPos(id);

}

void CAOIGUIDefectViewGrayScaleDlg::moveRightButton(int id)
{
	CString str;
	str.Format(L"%d", id);

	m_edtMax.SetWindowText(str);
	m_spinMaxValue = id;
	//m_spinMax.SetPos(id);
}

void CAOIGUIDefectViewGrayScaleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAOIGUIDefectViewGrayScaleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		CPaintDC dc(this);
		CRect rect;
		GetClientRect(&rect);

		CDC memDC;
		memDC.CreateCompatibleDC(&dc);
		CBitmap bitmap;
		bitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
		CBitmap* oldBitmap = memDC.SelectObject(&bitmap);
		Gdiplus::SolidBrush brush(Gdiplus::Color(196, 196, 196));

		Gdiplus::Graphics gc(memDC.GetSafeHdc());
		//gc.FillRectangle(&brush, rect.left, rect.top, rect.Width(), rect.Height());

		Gdiplus::Font font(L"Arial", 8, Gdiplus::FontStyleBold);
		Gdiplus::SolidBrush brushText(Color(255, 38, 140, 231));

		gc.DrawString(L"Feature Grayscale Threshold", -1, &font, Gdiplus::PointF(10, 10), &brushText);

		dc.BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

		memDC.SelectObject(oldBitmap);
		memDC.DeleteDC();
		bitmap.DeleteObject();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAOIGUIDefectViewGrayScaleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	AfxMessageBox(L"click arrow button");
}


void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dis(0, 255);

	int v = dis(gen);

	byte v2 = (byte)v;

	m_slider->ExpandFromValue(v2);
}


void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dis(0, 255);

	int v = dis(gen);

	byte v2 = (byte)v;

	m_slider->ReduceFromValue(v2);
}


void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	m_slider->Expand();
}


void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton5()
{
	// TODO: Add your control notification handler code here
	m_slider->Reduce();
}


void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton6()
{
	// TODO: Add your control notification handler code here
	m_slider->Reversal();
}


void CAOIGUIDefectViewGrayScaleDlg::OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	if (pNMUpDown->iDelta < 0 && m_spinMinValue == 0)
		return;

	if (pNMUpDown->iDelta < 0)
		m_spinMinValue--;
	else
		m_spinMinValue++;

	CString strVal;
	strVal.Format(L"%d", m_spinMinValue);

	m_edtMin.SetWindowText(strVal);
	m_slider->SetMin(m_spinMinValue);
	*pResult = 0;
}


void CAOIGUIDefectViewGrayScaleDlg::OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	if (pNMUpDown->iDelta > 0 && m_spinMaxValue == 255)
		return;

	if (pNMUpDown->iDelta < 0)
		m_spinMaxValue--;
	else
		m_spinMaxValue++;

	CString strVal;
	strVal.Format(L"%d", m_spinMaxValue);

	m_edtMax.SetWindowText(strVal);
	m_slider->SetMax(m_spinMaxValue);
	*pResult = 0;
}

void CAOIGUIDefectViewGrayScaleDlg::OnEnterAfterInputMinValue()
{
	CString str;
	m_edtMin.GetWindowText(str);

	int v = _ttoi(str);
	m_slider->SetMin(v);
	m_spinMin.SetPos(v);
}
void CAOIGUIDefectViewGrayScaleDlg::OnEnterAfterInputMaxValue()
{
	CString str;
	m_edtMax.GetWindowText(str);

	int v = _ttoi(str);

	m_slider->SetMax(v);
	m_spinMax.SetPos(v);
}
BOOL CAOIGUIDefectViewGrayScaleDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		CWnd* pWnd = GetFocus();
		if (pWnd->GetDlgCtrlID() == m_edtMin.GetDlgCtrlID())
		{
			OnEnterAfterInputMinValue();
		}
		if (pWnd->GetDlgCtrlID() == m_edtMax.GetDlgCtrlID())
		{
			OnEnterAfterInputMaxValue();
		}
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CAOIGUIDefectViewGrayScaleDlg::OnBnClickedButton7()
{
	// TODO: Add your control notification handler code here
	m_imagePlanes->CreateThumbnailImage(L"C:\\temp\\1_C-NET2_1\\");
}
