
#include "pch.h"
#include <Commctrl.h>

#include "ThumbnailItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CThumbnailItem, CViewBase)
CThumbnailItem::CThumbnailItem() :  CViewBase(), m_bTracking(false), m_bMouseClicked(false), m_boarderColor(Gdiplus::Color(255, 0, 0, 255)), m_fontDrawText(L"Arial", 8), m_colorText(Gdiplus::Color(255, 0, 0, 0)), m_colorTextArea(Gdiplus::Color(255, 255, 255, 255))
{
}



CThumbnailItem::~CThumbnailItem()
{
}


BEGIN_MESSAGE_MAP(CThumbnailItem, CViewBase)
	//{{AFX_MSG_MAP(CThumbnailItem)
	//}}AFX_MSG_MAP
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_WM_MOUSELEAVE()
    ON_WM_MOUSEMOVE()
    ON_WM_MOUSEHOVER()
END_MESSAGE_MAP()


void CThumbnailItem::OnDrawBoarder(Gdiplus::Graphics& gr)
{
    CRect rect;
    GetClientRect(&rect);
    
    Gdiplus::RectF rectRender;
    rectRender.X = rect.left;
    rectRender.Y = rect.top;
    rectRender.Width = rect.Width();
    rectRender.Height = rect.Height();

    Pen pen(m_bMouseClicked ? m_boarderColor : GetBkColor(), m_boardThickness * 2);
    gr.DrawRectangle(&pen, rectRender);
}

void CThumbnailItem::OnDrawImage(Gdiplus::Graphics& gr)
{
    gr.DrawImage(m_image.get(), m_rectImage);
}

void CThumbnailItem::OnDrawBackground(Gdiplus::Graphics& gr)
{
    Gdiplus::SolidBrush brush(Gdiplus::Color(255, 0, 0, 0));

    Gdiplus::Rect rect;
    rect.X = m_rectImage.X;
    rect.Y = m_rectImage.Y;
    rect.Width = m_rectImage.Width;
    rect.Height = m_rectImage.Height;

    gr.FillRectangle(&brush, rect.X, rect.Y, rect.Width, rect.Height);
}


void CThumbnailItem::OnDrawText(Gdiplus::Graphics& gr)
{
    Gdiplus::Font font(m_fontDrawText.fontName.c_str(), m_fontDrawText.size, Gdiplus::FontStyleBold);
    Gdiplus::StringFormat format;
    format.SetAlignment(StringAlignmentCenter);
    format.SetLineAlignment(StringAlignmentCenter);
    format.SetFormatFlags(Gdiplus::StringFormatFlagsNoWrap);
    format.SetTrimming(Gdiplus::StringTrimmingEllipsisCharacter);

    Gdiplus::SolidBrush textColor(m_colorText);
    Gdiplus::SolidBrush textAreaColor(m_colorTextArea);

    gr.FillRectangle(&textAreaColor, m_rectName.X, m_rectName.Y, m_rectName.Width, m_rectName.Height);
    gr.DrawString(m_name, -1, &font, m_rectName, &format, &textColor);
}

void CThumbnailItem::OnCustomDraw(Gdiplus::Graphics& gr)
{
    OnDrawBoarder(gr);
    OnDrawBackground(gr);
    OnDrawImage(gr);
    OnDrawText(gr);
}


void CThumbnailItem::ResetTrackFlag( void )
{
    m_bTracking = false;
    TRACKMOUSEEVENT tme;  // tracking information

    memset( &tme, 0, sizeof(TRACKMOUSEEVENT) );
    tme.cbSize = sizeof(TRACKMOUSEEVENT);
    tme.dwFlags = TME_CANCEL;
    tme.hwndTrack = m_hWnd;

    _TrackMouseEvent(  &tme  );
    Invalidate();
}


void CThumbnailItem::OnLButtonDown(UINT nFlags, CPoint point)
{
    // TODO: Add your message handler code here and/or call default

    __super::OnLButtonDown(nFlags, point);
}


void CThumbnailItem::OnLButtonUp(UINT nFlags, CPoint point)
{
    // TODO: Add your message handler code here and/or call default
    m_bMouseClicked = true;
    Invalidate();

    click(GetDlgCtrlID());

    __super::OnLButtonUp(nFlags, point);
}


BOOL CThumbnailItem::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
    UNREFERENCED_PARAMETER(lpszClassName);

    BOOL bResult = __super::Create(AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS, ::LoadCursor(NULL, IDC_ARROW), (HBRUSH) ::GetStockObject(WHITE_BRUSH), ::LoadIcon(NULL, IDI_APPLICATION))
        , lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

    


    CRect rectWindow;
    GetWindowRect(&rectWindow);

    const int nameHeight = rectWindow.Height() * 0.2;

    m_rectImage.X = m_boardThickness;
    m_rectImage.Y = m_boardThickness;
    m_rectImage.Width = rectWindow.Width() - m_boardThickness * 2;
    m_rectImage.Height = rectWindow.Height() - nameHeight;

    m_rectName.X = m_boardThickness;
    m_rectName.Y = m_rectImage.GetBottom();
    m_rectName.Width = rectWindow.Width() - m_boardThickness * 2;
    m_rectName.Height = nameHeight - m_boardThickness * 2;

    return bResult;
}
void CThumbnailItem::SetMouseClicked(bool click)
{
    m_bMouseClicked = click;
    Invalidate();
}


void CThumbnailItem::OnMouseLeave()
{
    // TODO: Add your message handler code here and/or call default
    /*CRect rect;
    GetWindowRect(&rect);
    GetParent()->ScreenToClient(&rect);*/

    
    //MoveWindow(rect.left + 2, rect.top + 2, rect.Width(), rect.Height());
    //Invalidate();
    

    m_bTracking = false;

    leave();
    __super::OnMouseLeave();
}


void CThumbnailItem::OnMouseMove(UINT nFlags, CPoint point)
{
    // TODO: Add your message handler code here and/or call default
    /*CRect rect;
    GetWindowRect(&rect);
    GetParent()->ScreenToClient(&rect);*/

    if (!m_bTracking)
    {
        TRACKMOUSEEVENT tme;
        memset(&tme, 0, sizeof(TRACKMOUSEEVENT));
        tme.cbSize = sizeof(TRACKMOUSEEVENT);
        tme.dwFlags = TME_LEAVE | TME_HOVER;
        tme.hwndTrack = m_hWnd;
        m_bTracking = _TrackMouseEvent(&tme);

        //MoveWindow(rect.left - 2, rect.top - 2, rect.Width(), rect.Height());
        //Invalidate();
    }

    __super::OnMouseMove(nFlags, point);
}


void CThumbnailItem::OnMouseHover(UINT nFlags, CPoint point)
{
    // TODO: Add your message handler code here and/or call default
    
    over(GetDlgCtrlID());
    __super::OnMouseHover(nFlags, point);
}

void CThumbnailItem::SetClickTrigger(std::function<void(int)> fClick)
{
    click = std::move(fClick);
}

void CThumbnailItem::SetOverTrigger(std::function<void(int)> fOver)
{
    over = std::move(fOver);
}

void CThumbnailItem::SetLeaveTrigger(std::function<void()> fLeave)
{
    leave = std::move(fLeave);
}