#pragma once

#define WM_RANGE_BUTTON_MOUSE_MOVE	(WM_USER + 1000)
#define WM_RANGE_BUTTON_LBUTTON_UP	(WM_USER + 1001)

#include "../Common/ViewBase.h"
class SliderRangeButton : public CViewBase
{
	DECLARE_DYNAMIC(SliderRangeButton)
// Construction
public:
	SliderRangeButton(bool isLeft);
	virtual ~SliderRangeButton();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);

protected:
	virtual void OnCustomDraw(Gdiplus::Graphics& gr);
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(255, 255, 255, 255); }
protected:
	//{{AFX_MSG(SliderRangeButton)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags,CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags,CPoint point);
	afx_msg void OnMouseMove(UINT nFlags,CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	int m_posXClicked;
	bool m_clicked;
	bool m_isLeft;
};
