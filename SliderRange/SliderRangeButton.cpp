#include "pch.h"
#include "SliderRangeButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(SliderRangeButton, CViewBase)

SliderRangeButton::SliderRangeButton(bool isLeft) : CViewBase(), m_clicked(false), m_isLeft(isLeft)
{
	
}

SliderRangeButton::~SliderRangeButton()
{
}

BEGIN_MESSAGE_MAP(SliderRangeButton, CViewBase)
	//{{AFX_MSG_MAP(SliderRangeButton)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL SliderRangeButton::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	BOOL bResult = __super::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	return bResult;
}

void SliderRangeButton::OnCustomDraw(Gdiplus::Graphics& gr)
{
}

void SliderRangeButton::OnPaint()
{
	CPaintDC dc(this);
	Gdiplus::Graphics gc(dc.GetSafeHdc());

	//Gdiplus::Pen pen(Gdiplus::Color(255, 0, 0, 0), 1);
	Gdiplus::SolidBrush brush(GetBkColor());

	Gdiplus::GraphicsPath path;

	CRect rc;
	GetClientRect(&rc);

	int iX0 = 0, iX1 = rc.right - 1;
	int iY0 = 0, iY1 = rc.right - 1, iY2 = rc.bottom - 1;

	if (m_isLeft)
	{
		Gdiplus::Point pts[] =
		{
			Gdiplus::Point(iX0,iY2),
			Gdiplus::Point(iX0,iY1),
			Gdiplus::Point(iX1,iY0),
			Gdiplus::Point(iX1,iY2)
		};

		path.AddPolygon(pts, 4);
		path.AddLines(pts, 4);
	}
	else
	{
		Gdiplus::Point pts[] = {
			Gdiplus::Point(iX0,iY2),
			Gdiplus::Point(iX0,iY0),
			Gdiplus::Point(iX1,iY1),
			Gdiplus::Point(iX1,iY2)
		};
		path.AddPolygon(pts, 4);
		path.AddLines(pts, 4);
	}

	path.SetFillMode(Gdiplus::FillModeWinding);
	//gc.DrawPath(&pen, &path);

	Gdiplus::Region rgn(&path);
	gc.FillRegion(&brush, &rgn);

	Gdiplus::Pen pen(Gdiplus::Color(255, 60, 60, 60), 1);

	gc.DrawPath(&pen, &path);
}

void SliderRangeButton::OnLButtonDown(UINT nFlags,CPoint pt)
{
	m_posXClicked = pt.x;

	m_clicked = true;
	
	SetCapture();

	__super::OnLButtonDown(nFlags,pt);
}

void SliderRangeButton::OnMouseMove(UINT nFlags,CPoint pt)
{
	if(m_clicked)
		GetParent()->PostMessage(WM_RANGE_BUTTON_MOUSE_MOVE, pt.x - m_posXClicked, m_isLeft);

	__super::OnMouseMove(nFlags,pt);
}

void SliderRangeButton::OnLButtonUp(UINT nFlags,CPoint pt)
{
	GetParent()->PostMessage(WM_RANGE_BUTTON_LBUTTON_UP, m_isLeft);

	m_clicked = false;

	ReleaseCapture();

	__super::OnLButtonUp(nFlags,pt);
}
