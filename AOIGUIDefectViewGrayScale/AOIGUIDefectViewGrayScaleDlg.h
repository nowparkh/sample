
// AOIGUIDefectViewGrayScaleDlg.h : header file
//

#pragma once
class GrayScaleSliderControl;
class DefectViewImagePlanes;
// CAOIGUIDefectViewGrayScaleDlg dialog
class CAOIGUIDefectViewGrayScaleDlg : public CDialogEx
{
	enum
	{
		IDC_VIEW_GRAYSCALE_SLIDER_CONTROL = 1000
	};

	std::shared_ptr< GrayScaleSliderControl> m_slider;

// Construction
	int m_spinMinValue{ 0 };
	int m_spinMaxValue{ 255 };
	std::shared_ptr< DefectViewImagePlanes> m_imagePlanes;
public:
	CAOIGUIDefectViewGrayScaleDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AOIGUIDEFECTVIEWGRAYSCALE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	void moveLeftButton(int id);
	void moveRightButton(int id);
	void OnEnterAfterInputMinValue();
	void OnEnterAfterInputMaxValue();


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtMin;
	CSpinButtonCtrl m_spinMin;
	CEdit m_edtMax;
	CSpinButtonCtrl m_spinMax;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButton7();
};
