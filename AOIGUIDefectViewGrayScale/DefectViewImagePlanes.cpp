// DefectViewImagePlanes.cpp : implementation file
//

#include "pch.h"
#include "DefectViewImagePlanes.h"
#include "DefectViewImagePlanesThumbnailItem.h"


// DefectViewImagePlanes

IMPLEMENT_DYNAMIC(DefectViewImagePlanes, CViewBase)

DefectViewImagePlanes::DefectViewImagePlanes()
{

}

DefectViewImagePlanes::~DefectViewImagePlanes()
{
}


BEGIN_MESSAGE_MAP(DefectViewImagePlanes, CViewBase)
END_MESSAGE_MAP()



// DefectViewImagePlanes message handlers

void DefectViewImagePlanes::OnCustomDraw(Gdiplus::Graphics& gr)
{
	CRect rect;
	GetClientRect(&rect);

	rect.left = rect.left + 180;
	rect.right = rect.left + 6;

	Gdiplus::SolidBrush brush(Color(255, 0, 0, 0));

	gr.FillRectangle(&brush, rect.left, rect.top, rect.Width(), rect.Height());

	Gdiplus::Font font(L"Arial", 10, Gdiplus::FontStyleBold);
	Gdiplus::SolidBrush textBrush(Color(255, 38, 140, 231));

	CString listTitle = L"Selected Image Planes";
	gr.DrawString(listTitle, -1, &font, Gdiplus::PointF(2, 10), &textBrush);

	CString thumbTitle = L"Image Plane List";
	gr.DrawString(thumbTitle, -1, &font, Gdiplus::PointF(190, 10), &textBrush);

	if (m_renderItem.get())
	{
		Gdiplus::Rect imageRect;
		imageRect.X = 500;
		imageRect.Y = 40;
		imageRect.Width = m_renderItem.get()->m_fmImage.get()->GetWidth();
		imageRect.Height = m_renderItem.get()->m_fmImage.get()->GetHeight();
		
		
		gr.DrawImage(m_renderItem->m_fmImage.get(), imageRect);
		
	}
}

void DefectViewImagePlanes::ItemClick(std::weak_ptr< CThumbnailItem> item, std::weak_ptr< CThumbnailItem> preItem)
{
	std::weak_ptr< DefectViewImagePlanesThumbnailItem> clickPreItem = std::dynamic_pointer_cast<DefectViewImagePlanesThumbnailItem>(preItem.lock());
	if (preItem.lock())
		clickPreItem.lock()->ClearFMImage();

	std::weak_ptr< DefectViewImagePlanesThumbnailItem> clickItem = std::dynamic_pointer_cast<DefectViewImagePlanesThumbnailItem>(item.lock());

	if (item.lock())
	{
		m_renderItem = clickItem.lock();
		m_renderItem->MakeFMImage();

		int index = m_selectImagePlanes.GetItemCount();

		m_selectImagePlanes.InsertItem(index, clickItem.lock()->GetGroup());
		m_selectImagePlanes.SetItem(index, 1, LVIF_TEXT, clickItem.lock()->GetName(), 0, 0, 0, 0);
	}

	Invalidate();


}

void DefectViewImagePlanes::ItemOver(std::weak_ptr< CThumbnailItem> item, std::weak_ptr< CThumbnailItem> preItem)
{
	std::weak_ptr< DefectViewImagePlanesThumbnailItem> preOverItem = std::dynamic_pointer_cast<DefectViewImagePlanesThumbnailItem>(preItem.lock());
	preOverItem.lock()->ClearFMImage();

	std::weak_ptr< DefectViewImagePlanesThumbnailItem> overItem = std::dynamic_pointer_cast<DefectViewImagePlanesThumbnailItem>(item.lock());
	overItem.lock();
	overItem.lock()->MakeFMImage();

		
}

std::vector<CString> GetAllFiles(CString fullPath)
{
	std::vector<CString> strArray;

	fullPath = fullPath + L"*";
	
	CFileFind finder;
	BOOL bWorking = finder.FindFile(fullPath);
	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (!finder.IsDots())
		{
			CString path = finder.GetFilePath();
			strArray.push_back(path);
		}
	}
	finder.Close();

	return strArray;
}

void DefectViewImagePlanes::CreateThumbnailImage(CString fullPath)
{
	m_controls.Clear();

	std::vector<CString> files = GetAllFiles(fullPath);

	for (int i = 0; i < files.size(); i++)
	{
		CString path = files[i];

		CFileFind finder;
		BOOL bWorking = finder.FindFile(path);

		if (bWorking)
		{
			finder.FindNextFile();

			CString name = finder.GetFileTitle();

			std::shared_ptr< DefectViewImagePlanesThumbnailItem> btn = std::make_shared< DefectViewImagePlanesThumbnailItem>(std::shared_ptr< Gdiplus::Bitmap>(Gdiplus::Bitmap::FromFile(path)), name);

			CString group;
			group.Format(L"%d", i + 1);

			btn->SetGroup(group);

			m_controls.Add(btn);
		}
		finder.Close();
	}
}

BOOL DefectViewImagePlanes::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	UNREFERENCED_PARAMETER(lpszClassName);

	BOOL bResult = __super::Create(AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS, ::LoadCursor(NULL, IDC_ARROW), (HBRUSH) ::GetStockObject(WHITE_BRUSH), ::LoadIcon(NULL, IDI_APPLICATION))
		, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	CRect rectThumbnail;
	GetClientRect(&rectThumbnail);
	rectThumbnail.left = rect.left + 186;
	rectThumbnail.top = 200;

	m_controls.Create(L"", L"", WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN, rectThumbnail, this, 15000);
	m_controls.SetClickTrigger(std::bind(&DefectViewImagePlanes::ItemClick, this, std::placeholders::_1, std::placeholders::_1));
	m_controls.SetOverTrigger(std::bind(&DefectViewImagePlanes::ItemOver, this, std::placeholders::_1, std::placeholders::_1));

	CRect rectList;
	GetClientRect(&rectList);
	rectList.right = 180;
	rectList.top = 40;

	m_selectImagePlanes.Create(WS_CHILD | WS_VISIBLE | LVS_REPORT, rectList, this, 15001);
	m_selectImagePlanes.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_selectImagePlanes.InsertColumn(0, L"Group", LVCFMT_LEFT, 60);
	m_selectImagePlanes.InsertColumn(1, L"Image Plane", LVCFMT_LEFT, 120);

	return bResult;
}