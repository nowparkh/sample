#pragma once


// DefectViewImagePlanesThumbnailItem
#include "../Thumbnail/ThumbnailItem.h"
class DefectViewImagePlanesThumbnailItem : public CThumbnailItem
{
	DECLARE_DYNAMIC(DefectViewImagePlanesThumbnailItem)

	friend class DefectViewImagePlanes;

public:
	DefectViewImagePlanesThumbnailItem(std::shared_ptr< Gdiplus::Bitmap> image, CString name);
	virtual ~DefectViewImagePlanesThumbnailItem();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);

	CString GetGroup() { return m_group; }
	CString GetImagePlane() { return m_imagePlane; }

	void SetGroup(const CString& group) { m_group = group; }
	void SetImagePlane(const CString& plane) { m_imagePlane = plane; }

	void MakeFMImage();
	void ClearFMImage();

protected:
	virtual void OnDrawImage(Gdiplus::Graphics& gr);
	virtual	void OnDrawText(Gdiplus::Graphics& gr);
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(226, 226, 226); }

private:
	//.......variant
	CString m_group;
	CString m_imagePlane;
	
	std::weak_ptr< std::vector<int>> m_vecGrayScaleRange;

	std::shared_ptr< Gdiplus::Bitmap> m_fmImage;

	std::shared_ptr<CToolTipCtrl> m_toolTip;
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};


