#pragma once

#include "../Common/ViewBase.h"

#define ON_TNB_LCLICKED _T( "WM_ON_TNB_LCLICKED_{D190C85B-217C-4a91-8684-0C523559C67D}" )
#define ON_TNB_RCLICKED _T( "WM_ON_TNB_RCLICKED_{0BBF61D0-8379-4470-B30A-C11854B9938C}" )

static const UINT UWM_ON_TNB_LCLICKED = ::RegisterWindowMessage( ON_TNB_LCLICKED );
static const UINT UWM_ON_TNB_RCLICKED = ::RegisterWindowMessage( ON_TNB_RCLICKED );

/////////////////////////////////////////////////////////////////////////////
// CThumbnailItem window


class CThumbnailItem : public CViewBase
{
	DECLARE_DYNAMIC(CThumbnailItem)

public:
	
	struct ThumnailItemFont
	{
		ThumnailItemFont(std::wstring font, int s)
		{
			fontName = font;
			size = s;
		}
		std::wstring fontName;
		int size;
	};

private:
	std::function<void(int)> click;
	std::function<void(int)> over;
	std::function<void()> leave;

private:
	bool    m_bTracking;

	CString m_fullPath;
protected:
	const int m_boardThickness{ 4 };
	bool    m_bMouseClicked;
	Gdiplus::Rect m_rectImage;
	Gdiplus::RectF m_rectName;
	CString m_name;
	std::shared_ptr< Gdiplus::Bitmap> m_image;

	Gdiplus::Color m_boarderColor;
	ThumnailItemFont m_fontDrawText;
	Gdiplus::Color m_colorText;
	Gdiplus::Color m_colorTextArea;

public:
	//CThumbnailItem(std::shared_ptr< Gdiplus::Image> image, CString name);
	CThumbnailItem();
	virtual ~CThumbnailItem();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
public:
	void  ResetTrackFlag( void );
	void SetClickTrigger(std::function<void(int)> fClick);
	void SetOverTrigger(std::function<void(int)> fHover);
	void SetLeaveTrigger(std::function<void()> fLeave);
	void SetMouseClicked(bool click);

	void SetBoardColor(Gdiplus::Color color) { m_boarderColor = color; }
	void SetFont(const std::wstring fontName, const int size) 
	{ 
		m_fontDrawText.fontName = fontName;
		m_fontDrawText.size = size;
	}
	void SetTextColor(Gdiplus::Color color) { m_colorText = color;  }
	void SetTextAreaColor(Gdiplus::Color color) { m_colorTextArea = color; }


	CString GetName() { return m_name; }

protected:
	virtual void OnCustomDraw(Gdiplus::Graphics& gr);
	virtual void OnDrawImage(Gdiplus::Graphics& gr);
	virtual void OnDrawText(Gdiplus::Graphics& gr);
	virtual void OnDrawBoarder(Gdiplus::Graphics& gr);
	virtual void OnDrawBackground(Gdiplus::Graphics& gr);
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(211, 211, 211); }
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CThumbnailItem)
	public:
	//}}AFX_VIRTUAL

	//*************************************
	//*  Generated message map functions	*
	//*************************************
  protected:
	//{{AFX_MSG(CThumbnailItem)

	//}}AFX_MSG
	  DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseHover(UINT nFlags, CPoint point);
};
