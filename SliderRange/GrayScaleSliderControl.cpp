// GrayScaleSliderControl.cpp : implementation file
//

#include "pch.h"
#include "GrayScaleSliderControl.h"
#include "SliderRangeButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(GrayScaleSliderControl, CViewBase)

GrayScaleSliderControl::GrayScaleSliderControl() : CViewBase()
{
}

GrayScaleSliderControl::~GrayScaleSliderControl()
{
}


BEGIN_MESSAGE_MAP(GrayScaleSliderControl, CViewBase)
	//{{AFX_MSG_MAP(GrayScaleSliderControl)
	ON_WM_SIZE()
	ON_MESSAGE(WM_RANGE_BUTTON_MOUSE_MOVE, &GrayScaleSliderControl::OnSliderRangeButtonMouseMove)
	ON_MESSAGE(WM_RANGE_BUTTON_LBUTTON_UP, &GrayScaleSliderControl::OnSliderRangeButtonLButtonUp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GrayScaleSliderControl message handlers

BOOL GrayScaleSliderControl::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{

	BOOL bResult = __super::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	CRect rectClient;
	GetClientRect(&rectClient);
	const int textHeight = 20;
	const int barHeight = 10;

	m_areaText.m_rectText = Gdiplus::RectF(0, rectClient.bottom - textHeight, rectClient.right - rectClient.left, textHeight);
	m_areaText.m_leftText = L"0";
	m_areaText.m_rightText = L"255";

	m_rectBar = Gdiplus::RectF(0, rectClient.bottom - textHeight - barHeight, rectClient.right - rectClient.left, barHeight);
	m_rectGraph = Gdiplus::RectF(0, 0, rectClient.right - rectClient.left, rectClient.bottom - textHeight - barHeight);

	CRect rectSliderBtn(0, 0, 0, 0);
	rectSliderBtn.right = 10;
	rectSliderBtn.bottom = 18;

	m_leftButton = std::make_shared< SliderRangeButton>(true);
	m_rightButton = std::make_shared< SliderRangeButton>(false);

	m_leftButton->Create(L"", L"", WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectSliderBtn, this, IDC_BUTTON_LEFT, nullptr);
	m_rightButton->Create(L"", L"", WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectSliderBtn, this, IDC_BUTTON_RIGHT, nullptr);

	m_sliderY = m_rectBar.GetTop() + (rectSliderBtn.bottom);

	m_vecGrayScaleRange = std::make_shared< std::vector<int>>();
	SetRange(0, 255, 4);

	return bResult;
}

void GrayScaleSliderControl::MoveWindow(CRect* pRect,BOOL bRepaint)
{
	CRect rect;
	CRect rc0 = *pRect;
	GetWindowRect(&rect);

	rc0.bottom = rc0.top + rect.Height();

	CWnd::MoveWindow(&rc0, bRepaint);
	if(m_init)	
		SetCurrentPos(m_currentLeftPos, m_currentRightPos, true, true);
}

void GrayScaleSliderControl::SetGraph(std::vector<int> v)
{
	m_vecGraph.swap(v);
	Invalidate();
}


void GrayScaleSliderControl::OnSize(UINT nType,int cx,int cy)
{
	CWnd::OnSize(nType,cx,cy);
	
	if(::IsWindow(m_hWnd)==0)	return;
	if (m_leftButton == nullptr || ::IsWindow(m_leftButton->m_hWnd) == 0) return;
	if (m_rightButton == nullptr || ::IsWindow(m_rightButton->m_hWnd) == 0) return;

	if(m_init)
		SetCurrentPos(m_currentLeftPos, m_currentRightPos, true, true);
}

void GrayScaleSliderControl::SetCurrentPos(int leftPos ,int rightPos ,bool bFst ,bool bSnd)
{
	CRect rect;
	GetClientRect(&rect);

	const int startPos= m_margin;
	const int endPos = rect.right- m_margin;

	const double dbOne = (endPos  - startPos) * 1.0 / (m_rangeSet.max - m_rangeSet.min);

	CRect rc;
	m_leftButton->GetWindowRect(&rc);
	const int w = rc.Width();
	const int h = rc.Height();

	rc.top = m_sliderY - w - 1;
	rc.bottom = rc.top + h;

	if (leftPos < m_rangeSet.min)
		leftPos = m_rangeSet.min;
	if (rightPos > m_rangeSet.max)
		rightPos = m_rangeSet.max;

	if(bFst)	
	{
		if(leftPos > rightPos)
			leftPos = rightPos;
	}

	if(bSnd)
	{
		if(rightPos < leftPos)
			rightPos = leftPos;
	}

	const int pixL = startPos + (int)((leftPos - m_rangeSet.min) * dbOne);
	const int pixR = startPos + (int)((rightPos - m_rangeSet.min) * dbOne);

	if(bFst)
	{
		
		m_currentLeftPos = leftPos;
		rc.right = pixL + 1;
		rc.left = rc.right - w;
		m_leftButton->MoveWindow(&rc);

		moveLeftButton(m_currentLeftPos);
	}

	if(bSnd)
	{
		m_currentRightPos = rightPos;
		rc.left = pixR;
		rc.right = rc.left + w;
		m_rightButton->MoveWindow(&rc);
		moveRightButton(m_currentRightPos);
	}

	SetGrayScaleRange();
}

std::weak_ptr< std::vector<int>> GrayScaleSliderControl::GetGrayScaleRange()
{
	return m_vecGrayScaleRange;
}

void GrayScaleSliderControl::SetGrayScaleRange()
{
	m_vecGrayScaleRange->clear();
	if (m_reversal)
	{
		for (int i = 0; i < m_currentLeftPos; i++)
			m_vecGrayScaleRange->push_back(i);

		for (int i = m_currentRightPos; i <= 255; i++)
			m_vecGrayScaleRange->push_back(i);
	}
	else
	{
		for (int i = m_currentLeftPos; i <= m_currentRightPos; i++)
			m_vecGrayScaleRange->push_back(i);
	}
}

int GrayScaleSliderControl::SetMin(int v)
{
	CRect rect;
	GetClientRect(&rect);

	m_currentLeftPos = v;
	SetCurrentPos(m_currentLeftPos, m_currentRightPos, true, false);

	RedrawWindow(&rect, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

	return m_currentLeftPos;
}
int GrayScaleSliderControl::SetMax(int v)
{
	CRect rect;
	GetClientRect(&rect);

	m_currentRightPos = v;
	SetCurrentPos(m_currentLeftPos, m_currentRightPos, false, true);

	RedrawWindow(&rect, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

	return m_currentRightPos;
}

void GrayScaleSliderControl::OnCustomDraw(Gdiplus::Graphics& gr)
{
	CRect rc;
	GetClientRect(&rc);

	Gdiplus::Font font(L"Arial", 10, Gdiplus::FontStyleBold);
	Gdiplus::SolidBrush brush(Color(255, 0, 0, 0));
	Gdiplus::StringFormat format;

	format.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);

	gr.DrawString(m_areaText.m_leftText, -1, &font, m_areaText.m_rectText, &format, &brush);

	format.SetAlignment(Gdiplus::StringAlignment::StringAlignmentFar);
	gr.DrawString(m_areaText.m_rightText, -1, &font, m_areaText.m_rectText, &format, &brush);

	float pixelPer = (rc.Width() - (m_margin * 2)) / 255.0;
	if (!m_vecGraph.empty())
	{
		Gdiplus::RectF rectBar = m_rectBar;
		rectBar.X = m_margin;
		rectBar.Width = pixelPer;
		//int loopCount = (m_rangeSet.max - m_rangeSet.min) / m_rangeSet.tick;
		int loopCount = m_vecGraph.size();
		for (int i = 0; i < loopCount; i++)
		{
			Gdiplus::SolidBrush solidBrush(Gdiplus::Color(i, i, i));
			gr.FillRectangle(&solidBrush, rectBar);

			rectBar.X = rectBar.X + pixelPer;
		}

		float max = *std::max_element(m_vecGraph.begin(), m_vecGraph.end());

		Gdiplus::RectF rectGraph = m_rectGraph;
		rectGraph.X = m_margin;
		rectGraph.Width = pixelPer;
		for (int i = 0; i < m_vecGraph.size(); i++)
		{
			float per = m_vecGraph[i] / max * 100;
			float pix = m_rectGraph.Height * per / 100;

			rectGraph.Y = m_rectGraph.Height - pix;
			rectGraph.Height = pix;
			Gdiplus::SolidBrush solidBrush(Gdiplus::Color(67, 67, 67));

			gr.FillRectangle(&solidBrush, rectGraph);

			rectGraph.X = rectGraph.X + pixelPer;
		}
	}

	if (m_reversal)
	{
		Gdiplus::RectF rectLeft;
		Gdiplus::RectF rectRight;

		rectLeft.X = m_margin;
		rectLeft.Y = 0;
		rectLeft.Width = ((m_currentLeftPos)* pixelPer);
		rectLeft.Height = m_rectGraph.Height + m_rectBar.Height;

		
		rectRight.X = m_margin + ((m_currentRightPos) * pixelPer);
		rectRight.Y = 0 ;
		rectRight.Width = (m_rangeSet.max - m_currentRightPos) * pixelPer;
		//rectRight.Width = ((m_currentLeftPos - 1) * pixelPer);
		rectRight.Height = m_rectGraph.Height + m_rectBar.Height;


		Gdiplus::SolidBrush solidBrush(Gdiplus::Color(100, 98, 170, 229));

		gr.FillRectangle(&solidBrush, rectLeft);
		gr.FillRectangle(&solidBrush, rectRight);
	}
	else
	{
		Gdiplus::RectF rectOver;
		rectOver.X = (m_currentLeftPos * pixelPer) + m_margin;
		rectOver.Y = 0;
		rectOver.Width = ((m_currentRightPos - m_currentLeftPos) * pixelPer);
		rectOver.Height = m_rectGraph.Height + m_rectBar.Height;

		Gdiplus::SolidBrush solidBrush(Gdiplus::Color(100, 98, 170, 229));

		gr.FillRectangle(&solidBrush, rectOver);
	}
	
}


bool GrayScaleSliderControl::SetRange(int min,int max,int tick)
{
	int dis = max - min;
	if(dis < 2)		return false;
	//if(dis % 2)		return false;
	if(tick < 1)		return false;
	//if(dis % tick)	return false;

	m_rangeSet.min = min;
	m_rangeSet.max = max;
	m_rangeSet.tick = tick;

	m_init = true;
	SetCurrentPos(m_rangeSet.min, m_rangeSet.max, true, true);
	RedrawWindow();

	return true;
}


LRESULT GrayScaleSliderControl::OnSliderRangeButtonLButtonUp(WPARAM wParam, LPARAM lParam)
{
	CRect rect;
	GetClientRect(&rect);
	RedrawWindow(&rect, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

	return 0L;
}

LRESULT GrayScaleSliderControl::OnSliderRangeButtonMouseMove(WPARAM wParam, LPARAM lParam)
{
	CRect rect;
	GetClientRect(&rect);

	const int startPos = m_margin;
	const int endPos = rect.right - m_margin;

	int move = (int)(((int)wParam) / ((endPos - startPos) * 1.0 / (m_rangeSet.max - m_rangeSet.min)));

	if (lParam)	
		m_currentLeftPos += move;
	else	
		m_currentRightPos += move;
	SetCurrentPos(m_currentLeftPos, m_currentRightPos, lParam, !lParam);


	RedrawWindow(&rect, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

	return 0L;
}

void GrayScaleSliderControl::Expand()
{
	
	PostMessage(WM_RANGE_BUTTON_MOUSE_MOVE, -m_rangeSet.tick, true);
	PostMessage(WM_RANGE_BUTTON_MOUSE_MOVE, m_rangeSet.tick, false);
}

void GrayScaleSliderControl::Reduce()
{
	PostMessage(WM_RANGE_BUTTON_MOUSE_MOVE, m_rangeSet.tick, true);
	PostMessage(WM_RANGE_BUTTON_MOUSE_MOVE, -m_rangeSet.tick, false);
}

void GrayScaleSliderControl::Reversal()
{
	m_reversal = !m_reversal;
	SetGrayScaleRange();
	Invalidate();
}

void GrayScaleSliderControl::ExpandFromValue(byte v)
{
	if (m_currentLeftPos > v)
		SetMin(v);

	if (m_currentRightPos < v)
		SetMax(v);
}

void GrayScaleSliderControl::ReduceFromValue(byte v)
{
	if (m_currentLeftPos < v)
		SetMin(v);
}

void GrayScaleSliderControl::SetMoveLeftButtonTrigger(std::function<void(int)> fMove)
{
	moveLeftButton = std::move(fMove);
}
void GrayScaleSliderControl::SetMoveRightButtonTrigger(std::function<void(int)> fMove)
{
	moveRightButton = std::move(fMove);
}