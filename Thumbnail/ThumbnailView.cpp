#include "pch.h"

#include "ThumbnailView.h"
#include "ThumbnailItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(CThumbnailView, CViewBase)

CThumbnailView::CThumbnailView() : CViewBase()
{
}

CThumbnailView::~CThumbnailView()
{
    m_mapThunailImages.clear();
}


BEGIN_MESSAGE_MAP(CThumbnailView, CViewBase)
	//{{AFX_MSG_MAP(CThumbNailControl)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEWHEEL()

	//}}AFX_MSG_MAP

END_MESSAGE_MAP()

BOOL CThumbnailView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
    UNREFERENCED_PARAMETER(lpszClassName);

    BOOL bResult = __super::Create(AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS, ::LoadCursor(NULL, IDC_ARROW), (HBRUSH) ::GetStockObject(WHITE_BRUSH), ::LoadIcon(NULL, IDI_APPLICATION))
        , lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

    return bResult;
}

void CThumbnailView::RecalButtonPos()
{
    CRect rect;
    GetClientRect( &rect );

    int nX=0;
    int nY=0;
    m_row =0;

   
    for(auto item : m_mapThunailImages)
    {
        if( nX == 0 )
            m_row++;

        std::weak_ptr< CThumbnailItem> btn = item.second;

        if (btn.lock())
        {
            btn.lock()->SetWindowPos(NULL,
                m_startX + m_seperatorPixel + nX,
                m_startY + m_seperatorPixel + nY,
                0, 0, SWP_NOSIZE | SWP_NOZORDER);

            CRect r;
            btn.lock()->GetClientRect(r);
            btn.lock()->RedrawWindow(&r, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

            nX += m_thumbWidth + m_seperatorPixel;

            if ((rect.Width() - nX) < 0.8 * (m_thumbWidth + m_seperatorPixel))
            {
                nX = 0;
                nY += m_thumbHeight + m_seperatorPixel;
            }
        }
        
    }

}

void CThumbnailView::ItemClick(int id)
{
    m_preSelectedItem = m_selectedItem;
    if (m_preSelectedItem.lock())
        m_preSelectedItem.lock()->SetMouseClicked(false);

    m_selectedItem = m_mapThunailImages[id];

    click(m_selectedItem, m_preSelectedItem);
}

void CThumbnailView::ItemOver(int id)
{
    m_preOveredItem = m_overedItem;
    m_overedItem = m_mapThunailImages[id];

    over(m_overedItem, m_preOveredItem);
}

void CThumbnailView::ItemLeave()
{
}

//void CThumbnailView::Add( const CString& sPath )
void CThumbnailView::Add(std::shared_ptr< CThumbnailItem> btn)
{
    
    //CWnd의 ID를 0으로 설정하고 윈도우의 ID를 가져오면 0이 아닌 다른 값을 가져오게되어 1부터 설정한다.
    UINT id = m_mapThunailImages.size() + 1;
    btn->Create(L"",
        L"",
        WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN ,
        CRect(0, 0, m_thumbWidth, m_thumbHeight), this, id);


    btn->SetClickTrigger(std::bind(&CThumbnailView::ItemClick, this, std::placeholders::_1));
    btn->SetOverTrigger(std::bind(&CThumbnailView::ItemOver, this, std::placeholders::_1));
    btn->SetLeaveTrigger(std::bind(&CThumbnailView::ItemLeave, this));
    
    m_mapThunailImages[id] = btn;

    RecalButtonPos();
    RecalScrollBars();
}

void CThumbnailView::Clear()
{
    m_mapThunailImages.clear();
}

void CThumbnailView::OnHScroll(UINT nSBCode, UINT /* nPos */ , CScrollBar* /* pScrollBar */)
{
    int nScrollPos = GetScrollPos32(SB_HORZ);

    switch( nSBCode )
    {
        case SB_LEFT:
            break;

        case SB_ENDSCROLL:
            RedrawWindow();
            break;

        case SB_LINELEFT :
            SetScrollPos32(SB_HORZ, nScrollPos - 1 );
            break;

        case SB_LINERIGHT:
            SetScrollPos32(SB_HORZ, nScrollPos + 1);
            break;

        case SB_PAGELEFT :
            SetScrollPos32(SB_HORZ, nScrollPos - 20 );
            break;

        case SB_PAGERIGHT:
            SetScrollPos32(SB_HORZ, nScrollPos + 20);
            break;

        case SB_RIGHT:
            break;

        case SB_THUMBPOSITION:  // Go down...
        case SB_THUMBTRACK:
            SetScrollPos32( SB_HORZ, GetScrollPos32(SB_HORZ, TRUE) );
            break;

        default:
            break;
    }

    m_startX = -GetScrollPos32(SB_HORZ);
    RecalButtonPos();
}

void CThumbnailView::RecalScrollBars()
{
    CRect rect;
    GetClientRect( &rect );
    ClientToScreen( &rect );

    long nTotalWidth  = m_thumbWidth * m_col + m_seperatorPixel * ( 1 + m_col);
    long nTotalHeight = m_thumbHeight * m_row + m_seperatorPixel * ( 2 + m_row);

    long widthDiff = nTotalWidth  - rect.Width();
    long heightDiff = nTotalHeight - rect.Height();

    if(widthDiff > m_seperatorPixel && m_mapThunailImages.size() >= m_col)
    {
        SCROLLINFO si;
        memset( &si, 0, sizeof(SCROLLINFO) );

        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_PAGE | SIF_RANGE;
        si.nPage = (int)(0.25*(widthDiff +14));
        si.nMin = 0;
        si.nMax = (int)(1.25*(widthDiff +14));

        SetScrollInfo( SB_HORZ, &si, TRUE );

        EnableScrollBarCtrl( SB_HORZ );
        EnableScrollBar( SB_HORZ );
    }

    if(heightDiff > m_seperatorPixel)
    {
        SCROLLINFO si;
        memset( &si, 0, sizeof(SCROLLINFO) );

        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_PAGE | SIF_RANGE;
        si.nPage = (int)(0.25*(heightDiff +14));
        si.nMin = 0;
        si.nMax = (int)(1.25*(heightDiff +14));

        SetScrollInfo( SB_VERT, &si, TRUE );

        EnableScrollBarCtrl( SB_VERT );
        EnableScrollBar( SB_VERT );
    }

}

BOOL CThumbnailView::SetScrollPos32(int nBar, int nPos, BOOL bRedraw)
{
    SCROLLINFO si;
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask  = SIF_POS;
    si.nPos   = nPos;
    return SetScrollInfo(nBar, &si, bRedraw);
}

int CThumbnailView::GetScrollPos32( int nBar, BOOL bGetTrackPos )
{
    SCROLLINFO si;
    si.cbSize = sizeof(SCROLLINFO);

    if (bGetTrackPos)
    {
        if (GetScrollInfo(nBar, &si, SIF_TRACKPOS))
            return si.nTrackPos;
    }
    else
    {
        if (GetScrollInfo(nBar, &si, SIF_POS))
            return si.nPos;
    }

    return 0;
}

void CThumbnailView::OnVScroll(UINT nSBCode, UINT /*nPos*/, CScrollBar* /*pScrollBar */)
{
    int nScrollPos = GetScrollPos32(SB_VERT);
    switch( nSBCode )
    {
        case SB_BOTTOM:
            break;

        case SB_ENDSCROLL:
            RedrawWindow();
            break;

        case SB_LINEDOWN:
            SetScrollPos32(SB_VERT, nScrollPos + 1 );
            break;

        case SB_LINEUP:
            SetScrollPos32(SB_VERT, nScrollPos - 1 );
            break;

        case SB_PAGEDOWN:
            SetScrollPos32(SB_VERT, nScrollPos + 20 );
            break;

        case SB_PAGEUP:
            SetScrollPos32(SB_VERT, nScrollPos - 20 );
            break;

        case SB_THUMBPOSITION: // Go down..
        case SB_THUMBTRACK:
            SetScrollPos32( SB_VERT, GetScrollPos32(SB_VERT, TRUE) );
            break;

        case SB_TOP:
            break;

        default:
            break;
    }

    m_startY = -GetScrollPos32(SB_VERT);
    RecalButtonPos();
}

BOOL CThumbnailView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    int nScrollPos = GetScrollPos32(SB_VERT);
    int nStep = zDelta/WHEEL_DELTA;

    SetScrollPos32(SB_VERT, nScrollPos - nStep * 20 );

    m_startY = -GetScrollPos32(SB_VERT);
    RecalButtonPos();

    return __super::OnMouseWheel(nFlags, zDelta, pt);
}

void CThumbnailView::OnCustomDraw(Gdiplus::Graphics& gr)
{

}


void CThumbnailView::SetClickTrigger(std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> fClick)
{
    click = std::move(fClick);
}

void CThumbnailView::SetOverTrigger(std::function<void(std::weak_ptr< CThumbnailItem>, std::weak_ptr< CThumbnailItem>)> fOver)
{
    over = std::move(fOver);
}