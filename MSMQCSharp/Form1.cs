﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Messaging;


namespace MSMQCSharp
{
    public partial class Form1 : Form
    {
        string m_queueName = @".\Private$\MSMQT";
        public class Order
        {
            public int orderId;
            public DateTime orderTime;
        };

        public Form1()
        {
            InitializeComponent();
        }

        public void SendMessage()
        {
            MessageQueue myQueue;
            if (MessageQueue.Exists(m_queueName))
                myQueue = new MessageQueue(m_queueName);
            else
                myQueue = MessageQueue.Create(m_queueName);

            myQueue.Send("test");
        }

        //**************************************************
        // Receives a message containing an Order.
        //**************************************************

        public void ReceiveMessage()
        {
            // Connect to the a queue on the local computer.
            //MessageQueue myQueue = MessageQueue.Create(@".\Private$\MSMQT");
            MessageQueue myQueue = new MessageQueue(m_queueName);

            myQueue.Formatter = new XmlMessageFormatter(new Type[]
                {typeof(string)});

            try
            {
                System.Messaging.Message myMessage = myQueue.Receive();

                string ooo = (string)myMessage.Body;
                //Order myOrder = (Order)myMessage.Body;

                // Display message information.
                //Console.WriteLine("Order ID: " +
                //    myOrder.orderId.ToString());
                //Console.WriteLine("Sent: " +
                //    myOrder.orderTime.ToString());
            }

            catch (MessageQueueException)
            {
                // Handle Message Queuing exceptions.
            }

            // Handle invalid serialization format.
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                myQueue.Close();
            }

            // Catch other exceptions as necessary.

        }

       
        private void btnSend_Click_1(object sender, EventArgs e)
        {
            SendMessage();
        }

        private void btnReceive_Click_1(object sender, EventArgs e)
        {
            ReceiveMessage();
        }
    }
}
