#include "pch.h"
#include "ViewBase.h"

IMPLEMENT_DYNAMIC(CViewBase, CWnd)

CViewBase::CViewBase() : CWnd()
{

}

CViewBase::~CViewBase()
{
}


BEGIN_MESSAGE_MAP(CViewBase, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CViewBase message handlers

BOOL CViewBase::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	UNREFERENCED_PARAMETER(lpszClassName);
	return __super::Create(AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW, ::LoadCursor(NULL, IDC_ARROW), (HBRUSH) ::GetStockObject(WHITE_BRUSH), ::LoadIcon(NULL, IDI_APPLICATION))
		, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

void CViewBase::OnPaint()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	CDC memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap* oldBitmap = memDC.SelectObject(&bitmap);
	Gdiplus::SolidBrush brush(GetBkColor());

	Gdiplus::Graphics gc(memDC.GetSafeHdc());
	gc.FillRectangle(&brush, rect.left, rect.top, rect.Width(), rect.Height());

	OnCustomDraw(gc);

	dc.BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(oldBitmap);
	memDC.DeleteDC();
}



BOOL CViewBase::OnEraseBkgnd(CDC* pDC)
{
	UNREFERENCED_PARAMETER(pDC);

	/*CRect rect;
	GetClientRect(&rect);

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* oldBitmap = memDC.SelectObject(&bitmap);
	Gdiplus::SolidBrush brush(GetBkColor());

	Gdiplus::Graphics gc(memDC.GetSafeHdc());
	gc.FillRectangle(&brush, rect.left, rect.top, rect.Width(), rect.Height());

	OnCustomEraseBkgnd(gc);

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(oldBitmap);
	memDC.DeleteDC();*/

	return FALSE;
}

