#pragma once

class SliderRangeButton;
#include "../Common/ViewBase.h"
class GrayScaleSliderControl : public CViewBase
{
	DECLARE_DYNAMIC(GrayScaleSliderControl)

	enum
	{
		IDC_BUTTON_LEFT = 1000,
		IDC_BUTTON_RIGHT
	};

public:
	GrayScaleSliderControl();
	virtual ~GrayScaleSliderControl();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	
	void MoveWindow(CRect*pRect ,BOOL bRepaint=TRUE);

	void SetGraph(std::vector<int> v);
	bool SetRange(int min, int max, int tick);

	void Expand();
	void Reduce();
	void Reversal();
	void ExpandFromValue(byte v);
	void ReduceFromValue(byte v);
	void SetGrayScaleRange();

	int SetMin(int v);
	int SetMax(int v);

	void SetMoveLeftButtonTrigger(std::function<void(int)> fMove);
	void SetMoveRightButtonTrigger(std::function<void(int)> fMove);

	std::weak_ptr< std::vector<int>> GetGrayScaleRange();
	
	std::function<void(int)> moveLeftButton;
	std::function<void(int)> moveRightButton;

protected:
	virtual void OnCustomDraw(Gdiplus::Graphics& gr);
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(226, 226, 226); }

private:
	void SetCurrentPos(int leftPos, int rightPos, bool bFst, bool bSnd);
protected:
	//{{AFX_MSG(GrayScaleSliderControl)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnSliderRangeButtonLButtonUp(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSliderRangeButtonMouseMove(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	struct RangeSet
	{
		int min{ 0 };
		int max{ 0 };
		int tick{ 0 };
	};

	RangeSet m_rangeSet;

	int m_currentLeftPos{ 0 };
	int m_currentRightPos{ 0 };

	int m_sliderY;

	int m_margin{ 10 };
	bool m_init{ false };

	std::vector<int> m_vecGraph;

	std::shared_ptr< SliderRangeButton> m_leftButton;
	std::shared_ptr< SliderRangeButton> m_rightButton;

	struct TextArea
	{
		Gdiplus::RectF m_rectText;
		CString m_leftText;
		CString m_rightText;
	};
	
	TextArea m_areaText;

	Gdiplus::RectF m_rectBar;
	Gdiplus::RectF m_rectGraph;

	bool m_reversal{ false };

	std::shared_ptr< std::vector<int>> m_vecGrayScaleRange;
};
