
// AOIGUIDefectViewGrayScale.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CAOIGUIDefectViewGrayScaleApp:
// See AOIGUIDefectViewGrayScale.cpp for the implementation of this class
//

class CAOIGUIDefectViewGrayScaleApp : public CWinApp
{
private:
	ULONG_PTR m_gdiplusToken;
public:
	CAOIGUIDefectViewGrayScaleApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAOIGUIDefectViewGrayScaleApp theApp;
