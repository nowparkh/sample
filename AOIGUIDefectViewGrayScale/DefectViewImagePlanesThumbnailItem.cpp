// DefectViewImagePlanesThumbnailItem.cpp : implementation file
//

#include "pch.h"
#include "DefectViewImagePlanesThumbnailItem.h"


// DefectViewImagePlanesThumbnailItem

IMPLEMENT_DYNAMIC(DefectViewImagePlanesThumbnailItem, CThumbnailItem)

DefectViewImagePlanesThumbnailItem::DefectViewImagePlanesThumbnailItem(std::shared_ptr< Gdiplus::Bitmap> image, CString name) : CThumbnailItem()
{
	m_image = image;
    m_name = name;
}

DefectViewImagePlanesThumbnailItem::~DefectViewImagePlanesThumbnailItem()
{
}


BEGIN_MESSAGE_MAP(DefectViewImagePlanesThumbnailItem, CThumbnailItem)
END_MESSAGE_MAP()



// DefectViewImagePlanesThumbnailItem message handlers



BOOL DefectViewImagePlanesThumbnailItem::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	BOOL result = __super::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	CRect rt;
	GetClientRect(&rt);
	m_toolTip = std::make_shared<CToolTipCtrl>();
	m_toolTip->Create(this);
	m_toolTip->AddTool(this, m_name, rt, 1);
	m_toolTip->SetMaxTipWidth(500);
	m_toolTip->SetDelayTime(TTDT_AUTOPOP, 20000);

	return result;
}

void DefectViewImagePlanesThumbnailItem::OnDrawText(Gdiplus::Graphics& gr)
{
	__super::OnDrawText(gr);

	Pen pen(Color(255, 0, 0, 0), 1);
	gr.DrawRectangle(&pen, m_rectName);
}

void DefectViewImagePlanesThumbnailItem::OnDrawImage(Gdiplus::Graphics& gr)
{	
	double ratioX = (double)m_rectImage.Width / m_image->GetWidth();
	double ratioY = (double)m_rectImage.Height / m_image->GetHeight();

	double ratio = min(ratioX, ratioY);

	int newWidth = m_image->GetWidth() * ratio;
	int newHeight = m_image->GetHeight() * ratio;

	int x = (m_rectImage.Width / 2) - newWidth / 2;
	int y = (m_rectImage.Height / 2) - newHeight / 2;

	gr.DrawImage(m_image.get(), Gdiplus::Rect(x + m_boardThickness, y + m_boardThickness, newWidth, newHeight));
}

void DefectViewImagePlanesThumbnailItem::ClearFMImage()
{
	m_fmImage.reset();
}

void DefectViewImagePlanesThumbnailItem::MakeFMImage()
{
	//
	//if (m_vecGrayScaleRange.lock() && !m_vecGrayScaleRange.expired() && m_vecGrayScaleRange.lock()->size() > 0)
	//{
	//	UINT w = m_image->GetWidth();
	//	UINT h = m_image->GetHeight();

	//	m_fmImage.reset(m_image->Clone(0, 0, m_image->GetWidth(), m_image->GetHeight(), PixelFormat32bppRGB));

	//	for (int i = 0; i < m_image->GetWidth(); i++)
	//	{
	//		for (int j = 0; j < m_image->GetHeight(); j++)
	//		{
	//			Gdiplus::Color color;
	//			m_fmImage->GetPixel(i, j, &color);

	//			BYTE r = color.GetR();
	//			BYTE g = color.GetG();
	//			BYTE b = color.GetB();

	//			auto itR = std::find(m_vecGrayScaleRange.lock()->begin(), m_vecGrayScaleRange.lock()->end(), r);
	//			auto itG = std::find(m_vecGrayScaleRange.lock()->begin(), m_vecGrayScaleRange.lock()->end(), g);
	//			auto itB = std::find(m_vecGrayScaleRange.lock()->begin(), m_vecGrayScaleRange.lock()->end(), b);

	//			if (itR != m_vecGrayScaleRange.lock()->end() && itG != m_vecGrayScaleRange.lock()->end() && itB != m_vecGrayScaleRange.lock()->end())
	//				m_fmImage->SetPixel(i, j, Gdiplus::Color(255, 255, 255));
	//		}
	//	}
	//}
}

BOOL DefectViewImagePlanesThumbnailItem::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	m_toolTip->RelayEvent(pMsg);
	return CThumbnailItem::PreTranslateMessage(pMsg);
}
