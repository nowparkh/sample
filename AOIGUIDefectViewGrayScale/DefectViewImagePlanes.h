#pragma once


// DefectViewImagePlanes
class DefectViewImagePlanesThumbnailItem;
#include "../Common/ViewBase.h"
#include "../Thumbnail/ThumbnailView.h"
class DefectViewImagePlanes : public CViewBase
{
	DECLARE_DYNAMIC(DefectViewImagePlanes)
private:
	CListCtrl m_selectImagePlanes;
	CThumbnailView m_controls;
public:
	DefectViewImagePlanes();
	virtual ~DefectViewImagePlanes();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	void CreateThumbnailImage(CString fullPath);

	void ItemClick(std::weak_ptr< CThumbnailItem> item, std::weak_ptr< CThumbnailItem> preItem);
	void ItemOver(std::weak_ptr< CThumbnailItem> item, std::weak_ptr< CThumbnailItem> preItem);

protected:
	virtual void OnCustomDraw(Gdiplus::Graphics& gr);
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(226, 226, 226); }

private:
	std::shared_ptr< DefectViewImagePlanesThumbnailItem> m_renderItem;

protected:
	DECLARE_MESSAGE_MAP()
};


