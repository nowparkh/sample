#pragma once

class CViewBase : public CWnd
{
	DECLARE_DYNAMIC(CViewBase)

public:
	CViewBase();
	virtual ~CViewBase();

	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void Refresh() {};
	virtual void RefreshUI() {};
protected:
	virtual void OnCustomDraw(Gdiplus::Graphics& gr) {};
	virtual Gdiplus::Color GetBkColor() { return Gdiplus::Color(255, 255, 255); }

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


